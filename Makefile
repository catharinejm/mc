CC = gcc
CFLAGS = -g -Wall

SOURCES=$(wildcard *.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))

.PHONY: all clean run

all: mc

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $^

mc: $(OBJECTS)
	$(CC) $(CFLAGS) -o mc $(OBJECTS)

clean:
	rm -f $(OBJECTS) mc

run: mc
	./mc
