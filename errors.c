/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

#include "mc.h"

char *error_msg(Error err) {
    switch (err) {
    case ERR_OK           : return "no error";
    case ERR_EOF          : return "premauture EOF";
    case ERR_INVALID      : return "invalid token";
    case ERR_RPAREN       : return "unmatched ')'";
    case ERR_RBRACK       : return "unmatched ']'";
    case ERR_RBRACE       : return "unmatched '}'";
    case ERR_NUM_TOO_BIG  : return "number literal is too large";
    case ERR_SYM_TOO_LONG : return "symbol is too long";
    case ERR_UNBOUND      : return "unbound symbol";
    case ERR_NEED_CONS    : return "expcected a cons cell";
    case ERR_BAD_DEF      : return "bad def, expected (def <symbol> <binding>?)";
    default:
        return "[unknown error]";
    }
}
