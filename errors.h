/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

#ifndef MC_ERRORS_H
#define MC_ERRORS_H

#include <setjmp.h>

typedef enum _Error {
    ERR_OK,
    ERR_EOF,
    ERR_INVALID,
    ERR_RPAREN,
    ERR_RBRACK,
    ERR_RBRACE,
    ERR_NUM_TOO_BIG,
    ERR_SYM_TOO_LONG,

    ERR_UNBOUND,
    ERR_NEED_CONS,
    ERR_BAD_DEF,

    _LAST_ERROR,
} Error;

EXTERN char *error_msg(Error err);

EXTERN jmp_buf JumpEnv;
#define ERROR_MESSAGE_LENGTH 256
EXTERN char ErrorMsg[ERROR_MESSAGE_LENGTH];

#define SET_ERROR_MSG(template, args...)                         \
    snprintf(ErrorMsg, ERROR_MESSAGE_LENGTH-1, template, args)

#define RECOVER() setjmp(JumpEnv)
#define ABORT(err) longjmp(JumpEnv, err)

#endif
