/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

#include "mc.h"

void eval_error(Obj o, Error err) {
    if (issymbol(o))
        SET_ERROR_MSG("ERROR: %s: %s", error_msg(err), (char*)o);
    else
        SET_ERROR_MSG("ERROR: %s", error_msg(err));
    ABORT(err);
}

Obj eval_list(Cons **env, Cons *lis);

Obj eval(Cons **env, Obj o) {
    if (NULL == env)
        fatal("null environment pointer!");

    if (NULL == o)
        return NULL;

    if (isnumber(o))
        return o;

    if (issymbol(o)) {
        int notfound;
        Obj res = alist_lookup(*env, o, &notfound);
        if (&notfound == res) {
            eval_error(o, ERR_UNBOUND);
            return NULL; // never reached
        }
        return res;
    }

    return eval_list(env, (Cons*)o);
}

Obj eval_list(Cons **env, Cons *lis) {
    if (S_def == lis->car) {
        Obj name = CADR(lis);
        if (!issymbol(name))
            eval_error(name, ERR_BAD_DEF);
        Obj value = eval(env, CAR(CDDR(lis)));
        Cons* name_cons = (Cons*)ExprBufMark;
        Cons* value_cons = (Cons*)(ExprBufMark + sizeof(Cons));
        ExprBufMark += 2*sizeof(Cons);
        name_cons->car = name;
        name_cons->cdr = value_cons;
        value_cons->car = value;
        value_cons->cdr = *env;
        *env = name_cons;
        return NULL;
    }
    return lis;
}

Obj alist_lookup(Obj alist, Obj key, Obj notfound) {
    while (alist != NULL && !issymbol(alist)) {
        Cons *c = (Cons*)alist;
        if (c->car == key) {
            if (NULL == c->cdr || issymbol(c->cdr))
                return c->cdr;
            return ((Cons*)c->cdr)->car;
        }
        if (NULL == c->cdr || issymbol(c->cdr))
            return notfound;
        alist = ((Cons*)c->cdr)->cdr;
    }

    return notfound;
}

bool issymbol(Obj o) {
    return o >= (void*)(SymbolTable) && o < (void*)(SymbolTableEnd);
}

bool isnumber(Obj o) {
    return IS_INT(o);
}
