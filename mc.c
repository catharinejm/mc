/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

#define EXTERN
  #include "mc.h"
#undef EXTERN

void dump_symbol_table();
void dump_env(Cons *env);

int main(int argc, char *argv[]) {
    InStream = stdin;
    FileName = "<stdin>";
    Line = 1;
    Col = 0;
    LastToken = (Token) {
        .tt = TT_INVALID,
        .line = 0,
        .st_col = 0,
        .st_col = 0,
        .value = -1,
    };
    LastChar = 0;

    init_symbol_table();

    memset(ExprBuf, 0, EXPR_BUF_LIMIT);
    ExprBufMark = ExprBuf;
    ExprBufEnd = ExprBuf + EXPR_BUF_LIMIT;

    memset(ErrorMsg, 0, ERROR_MESSAGE_LENGTH);

    int err_res;
    Cons *env = NULL;
    do {
        err_res = RECOVER();
        // ExprBufMark = ExprBuf;
        if (0 == err_res) {
            printf("> ");
            Obj expr = read();
            printf("read: ");
            mc_println(stdout, expr);
            Obj result = eval(&env, expr);
            printf("eval: ");
            mc_println(stdout, result);
        } else {
            fprintf(stderr, "%s\n", ErrorMsg);
        }
    } while (err_res != ERR_EOF);

    dump_symbol_table();
    dump_env(env);

    return 0;
}

void dump_symbol_table() {
    printf("Defined Symbols (%d/%d):\n", CurSymIdx, SYMBOL_LIMIT);
    for (int i = 0; i < CurSymIdx; i++) {
        printf("\t%s\n", SymbolTable[i]);
    }
}

void dump_env(Cons *env) {
    printf("Env:\n");
    mc_println(stdout, env);
}
