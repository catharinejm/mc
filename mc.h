/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

#ifndef MC_H
#define MC_H

#ifndef __LP64__
  #error mc requires 64-bit wordsize
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <stdbool.h>

#ifndef EXTERN
  #define EXTERN extern
#endif

#include "errors.h"
#include "symbols.h"

typedef enum _TokenTag {
    TT_INVALID,

    TT_LPAREN, TT_RPAREN,
    TT_LBRACK, TT_RBRACK,
    TT_LBRACE, TT_RBRACE,

    TT_SYMBOL, TT_NUMBER,
} TokenTag;

typedef struct _Token {
    TokenTag tt;
    int line;
    int st_col;
    int end_col;
    intptr_t value;
} Token;

typedef void* Obj;

EXTERN void error(char const *msg);
EXTERN void fatal(char const *msg);
EXTERN int nextc();
EXTERN void unreadc(int c);
EXTERN void eatwhitespace();

EXTERN bool issymchar(int c);
EXTERN bool isgroupchar(int c);

EXTERN Token next();
EXTERN void unreadtok(Token t);

EXTERN char *intern(char const *sym);

EXTERN Obj read();

EXTERN void mc_print(FILE *out, Obj o);
EXTERN void mc_println(FILE *out, Obj o);

/* 61-bit fixnum is 19 digits in base 10 */
#define MAX_NUMBER_LEN 20
#define FIXNUM_MAX (INTPTR_MAX >> 3)
#define FIXNUM_MIN (INTPTR_MIN >> 3)

#define MAX_SYM_LEN 32
#define SYMBOL_LIMIT 8192
#define EXPR_BUF_LIMIT (1 << 10)

EXTERN FILE *InStream;
EXTERN char const *FileName;
EXTERN int Line;
EXTERN int Col;
EXTERN Token LastToken;
EXTERN int LastChar;

EXTERN int CurSymIdx;
EXTERN char SymbolTable[MAX_SYM_LEN][SYMBOL_LIMIT];
EXTERN char *SymbolTableEnd;

EXTERN char ExprBuf[EXPR_BUF_LIMIT];
EXTERN char *ExprBufMark;
EXTERN char *ExprBufEnd;

#define INT_MASK 1
#define IS_INT(n) ((intptr_t)(n) & INT_MASK)
#define PACK_INT(raw) (((intptr_t)(raw) << 3) | INT_MASK)
#define UNPACK_INT(packed) ((intptr_t)(packed) >> 3)

typedef struct _Cons {
    Obj car;
    Obj cdr;
} Cons;

#define IF_CONS(o, block) \
    (NULL != o && !issymbol(o) ? (block) : NULL)
#define CAR(o) \
    IF_CONS(o, (((Cons*)(o))->car))
#define CDR(o) \
    IF_CONS(o, (((Cons*)(o))->cdr))

#define CAAR(o) \
    CAR(CAR(o))
#define CADR(o) \
    CAR(CDR(o))
#define CDAR(o) \
    CDR(CAR(o))
#define CDDR(o) \
    CDR(CDR(o))

EXTERN Obj eval(Cons **env, Obj o);
EXTERN Obj alist_lookup(Obj alist, Obj key, Obj notfound);
EXTERN bool issymbol(Obj o);
EXTERN bool isnumber(Obj o);

#endif
