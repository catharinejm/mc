/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

#include "mc.h"

void print_list_inner(FILE *out, Cons *o);

void mc_print(FILE *out, Obj o) {
    if (NULL == o) {
        fprintf(out, "()");
    }
    else if (isnumber(o)) {
        fprintf(out, "%li", UNPACK_INT(o));
    }
    else if (issymbol(o)) {
        fprintf(out, "%s", (char*)o);
    }
    else {
        fprintf(out, "(");
        print_list_inner(out, o);
        fprintf(out, ")");
    }
}

void print_list_inner(FILE *out, Cons *o) {
    for (;;) {
        mc_print(out, o->car);
        if (NULL == o->cdr)
            break;
        fprintf(out, " ");
        // if (issymbol(o->cdr)) {
        //     fprintf(out, ". ");
        //     mc_print(out, o->cdr);
        //     break;
        // }
        o = (Cons*)(o->cdr);
    }
}

void mc_println(FILE *out, Obj o) {
    mc_print(out, o);
    fprintf(out, "\n");
}
