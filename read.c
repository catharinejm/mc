/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

#include "mc.h"

void tok_error(Token tok, Error err) {
    SET_ERROR_MSG("ERROR (%s:%d:%d): %s",
                  FileName, tok.line, tok.st_col, error_msg(err));
    ABORT(err);
}

void fatal(char const *msg) {
    fprintf(stderr, "FATAL ERROR: %s\n", msg);
    exit(1);
}

int nextc() {
    int c;
    if (LastChar != 0) {
        c = LastChar;
        LastChar = 0;
    }
    else {
        c = getc(InStream);
        Col++;
        if (c == '\n') {
            Line++;
            Col = 0;
        }
    }
    return c;
}

void unreadc(int c) {
    LastChar = c;
}

void eatwhitespace() {
    int c;
    while(isspace(c = nextc()));
    unreadc(c);
}

bool isgroupchar(int c) {
    switch (c) {
    case '(':
    case ')':
    case '[':
    case ']':
    case '{':
    case '}':
        return 1;
    }
    return 0;
}

bool issymchar(int c) {
    return !isspace(c) && !isgroupchar(c);
}

Token next_number_or_symbol(Token tok, char c);

Token next() {
    if (LastToken.tt != TT_INVALID) {
        Token tok = LastToken;
        LastToken.tt = TT_INVALID;
        return tok;
    }
    eatwhitespace();
    int c = nextc();
    Token tok = {
        .tt = TT_INVALID,
        .line = Line,
        .st_col = Col,
        .end_col = Col+1,
        .value = -1,
    };
    if (c == EOF) {
        tok_error(tok, ERR_EOF);
        return tok;
    }
    switch (c) {
    case '(':
        tok.tt = TT_LPAREN;
        break;
    case ')':
        tok.tt = TT_RPAREN;
        break;
    case '[':
        tok.tt = TT_LBRACK;
        break;
    case ']':
        tok.tt = TT_RBRACK;
        break;
    case '{':
        tok.tt = TT_LBRACE;
        break;
    case '}':
        tok.tt = TT_RBRACE;
        break;
    case '-':
    case '+':
        ;;
        int c2 = nextc();
        unreadc(c2);
        if (!issymchar(c2)) {
            tok.tt = TT_SYMBOL;
            tok.value = (intptr_t)intern((char[2]){c, 0});
            break;
        }
        // fallthrough
    default:
        tok = next_number_or_symbol(tok, c);
    }
    return tok;
}

int extract_symbol(char buf[MAX_SYM_LEN], char c, bool *is_num_out) {
    memset(buf, 0, MAX_SYM_LEN);
    buf[0] = c;
    bool is_num = '-' == c || '+' == c || isdigit(c);
    int i = 1;
    while (issymchar(c = nextc())) {
        if (i < MAX_SYM_LEN)
            buf[i] = c;
        if (is_num && !isdigit(c))
            is_num = false;
        i++;
    }
    unreadc(c);
    if (is_num_out != NULL)
        *is_num_out = is_num;
    return i;
}

Token next_number_or_symbol(Token tok, char c) {
    char buf[MAX_SYM_LEN];
    bool is_num;
    int len = extract_symbol(buf, c, &is_num);
    tok.end_col = tok.st_col + len;
    if (len >= MAX_SYM_LEN)
        tok_error(tok, is_num ? ERR_NUM_TOO_BIG : ERR_SYM_TOO_LONG);
    if (is_num) {
        intptr_t num = strtol(buf, NULL, 10);
        if (num > FIXNUM_MAX || num < FIXNUM_MIN)
            tok_error(tok, ERR_NUM_TOO_BIG);
        tok.tt = TT_NUMBER;
        tok.value = PACK_INT(num);
    }
    else {
        tok.tt = TT_SYMBOL;
        tok.value = (intptr_t)intern(buf);
    }
    return tok;
}

void unreadtok(Token tok) {
    LastToken = tok;
}

char *intern(char const *sym) {
    if (CurSymIdx == SYMBOL_LIMIT)
        fatal("symbol table overflow");

    for (int i = 0; i < CurSymIdx; i++)
        if (sym[0] == SymbolTable[i][0] && !strcmp(sym, SymbolTable[i]))
            return (char*)(SymbolTable + i);

    /* Symbol table is zeroed so leave last byte alone.
       Overlong symbols will be truncated.
    */
    strncpy(SymbolTable[CurSymIdx], sym, MAX_SYM_LEN-1);
    return (char*)(SymbolTable + CurSymIdx++);
}

Obj read_list(TokenTag closer);

Obj read() {
    Token tok = next();
    Obj expr = NULL;
    switch (tok.tt) {
    case TT_INVALID:
        tok_error(tok, ERR_INVALID);
        break;
    case TT_LPAREN:
        expr = read_list(TT_RPAREN);
        break;
    case TT_LBRACK:
        expr = read_list(TT_RBRACK);
        break;
    case TT_LBRACE:
        expr = read_list(TT_RBRACE);
        break;
    case TT_SYMBOL:
        expr = (char*)tok.value;
        break;
    case TT_NUMBER:
        expr = (void*)tok.value;
        break;
    case TT_RPAREN:
        tok_error(tok, ERR_RPAREN);
        break;
    case TT_RBRACK:
        tok_error(tok, ERR_RBRACK);
        break;
    case TT_RBRACE:
        tok_error(tok, ERR_RBRACE);
        break;
    default:
        fatal("unreachable in read()");
    }
    return expr;
}

Obj read_list(TokenTag closer) {
    Token tok = next();
    switch (tok.tt) {
    case TT_INVALID:
        fatal("got invalid token from next()");
        return NULL;
    case TT_RPAREN:
    case TT_RBRACK:
    case TT_RBRACE:
        if (tok.tt == closer) {
            return NULL;
        }
        // fallthrough
    default:
        unreadtok(tok);
        Cons *cons = (void*)(ExprBufMark);
        ExprBufMark += sizeof(Cons);
        cons->car = read();
        cons->cdr = read_list(closer);
        return cons;
    }
}
