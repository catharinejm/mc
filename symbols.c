/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

#include "mc.h"

void init_symbol_table() {
    memset(SymbolTable, 0, SYMBOL_LIMIT * MAX_SYM_LEN);
    SymbolTableEnd = (void*)(SymbolTable) + SYMBOL_LIMIT * MAX_SYM_LEN;

    *(void**)(&S_def) = &SymbolTable[0];
    strcpy((char*)&SymbolTable[0], "def");

    CurSymIdx = 1;
}
