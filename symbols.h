/**
 * mc - metacircular lisp
 * Copyright 2019 Jonathan M Distad
 */

EXTERN void init_symbol_table();

EXTERN char const * const S_def;
